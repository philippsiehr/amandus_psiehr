#include <apps.h>
#include <boost/scoped_ptr.hpp>
#include <deal.II/integrators/l2.h>
#include <deal.II/integrators/laplace.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/algorithms/newton.h>

namespace Eigen
{
  /**
   * Integrator for matrix representation of the operator
   * \f[
   * F: H^1 \to (H^1)' \\
   * F(u) = v \mapsto (\nabla u, \nabla v) - \lambda (u, v)
   * \f]
   */
  template <int dim>
   class OperatorMatrix : public AmandusIntegrator<dim>
  {
    public:
      OperatorMatrix(double eigenvalue) : eigenvalue(eigenvalue)
    {
      this->use_cell = true;
      this->use_face = false;
      this->use_boundary = false;
    }

      virtual void cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
                        dealii::MeshWorker::IntegrationInfo<dim>& info) const
      {
        dealii::LocalIntegrators::Laplace::cell_matrix(dinfo.matrix(0).matrix,
                                                       info.fe_values(0));
        dealii::LocalIntegrators::L2::mass_matrix(dinfo.matrix(0).matrix,
                                                  info.fe_values(0),
                                                  -1.0 * this->eigenvalue);
      }

    private:
      double eigenvalue;
  };


  /**
   * Integrator for action of the operator
   * \f[
   * F: H^1 \to (H^1)' \\
   * F(u) = v \mapsto (\nabla u, \nabla v) - \lambda (u, v)
   * \f]
   */
  template <int dim>
   class OperatorAction : public AmandusIntegrator<dim>
  {
    public:
      OperatorAction(double eigenvalue) : eigenvalue(eigenvalue)
    {
      this->use_cell = true;
      this->use_face = false;
      this->use_boundary = false;
    }

      virtual void cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
                        dealii::MeshWorker::IntegrationInfo<dim>& info) const
      {
        dealii::LocalIntegrators::Laplace::cell_residual(dinfo.vector(0).block(0),
                                                         info.fe_values(0),
                                                         info.gradients[0][0]);
        dealii::LocalIntegrators::L2::L2(dinfo.vector(0).block(0),
                                         info.fe_values(0),
                                         info.values[0][0],
                                         -1.0 * this->eigenvalue);
      }

    private:
      double eigenvalue;
  };
}

template <int dim>
class Eigenfunction : public dealii::Function<dim>
{
  public:
    virtual double value(const dealii::Point<2>& p,
                         const unsigned int component = 0) const
    {
      return 10000 * std::sin(p(0)) * std::sin(p(1));
    }
};

template <int d>
void run(AmandusParameters& param)
{
  // Triangulation and FiniteElement
  param.enter_subsection("Discretization");

  dealii::Triangulation<d> tr;
  dealii::GridGenerator::hyper_cube(tr, 0.0, dealii::numbers::PI);
  tr.refine_global(param.get_integer("Refinement"));

  boost::scoped_ptr<const dealii::FiniteElement<d> >
    fe(dealii::FETools::get_fe_by_name<d, d>(param.get("FE")));

  param.leave_subsection();
  // Integrators for the model
  param.enter_subsection("Model");

  Eigen::OperatorMatrix<d> matrix_integrator(param.get_double("Eigenvalue"));
  Eigen::OperatorAction<d> action_integrator(param.get_double("Eigenvalue"));

  param.leave_subsection();
  // Amandus application (infrastructure for linear solver)
  param.enter_subsection("Application");

  AmandusApplicationSparse<d> app(tr, *fe, param.get_bool("UMFPack"));
  app.set_boundary(0);
  const unsigned int n_loops = param.get_integer(
      "Number of global refinement loops");
  const unsigned int n_qpoints = param.get_integer(
      "Number of quadrature points for boundary projection");

  param.leave_subsection();
  // Operators and algorithms

  app.parse_parameters(param);
  // inverse of the operator represented by the matrix assembled by
  // matrix_integrator
  AmandusSolve<d> linear_solver(app, matrix_integrator); 
  // the operator for which we are searching a root
  AmandusResidual<d> operator_action(app, action_integrator);
  // newton's method to find a root, using the operator and the inverse of
  // the operator's derivative, i.e.
  // u_{k+1} = u_k - linear_solver(operator_action(u_k))
  dealii::Algorithms::Newton<dealii::Vector<double> > newton(operator_action,
                                                             linear_solver);
  newton.parse_parameters(param);
  // tell the action integrator how to find its operand
  action_integrator.input_vector_names.push_back("Newton iterate");

  // vector that serves as starting value for newton's method and contains
  // the result of newton's method afterwards
  dealii::Vector<double> solution;
  // boundary values
  dealii::ZeroFunction<d> bdry_function;
  // other boundary examples
  //dealii::Functions::CosineFunction<d> bdry_function;
  //Eigenfunction<d> bdry_function;
  dealii::QGauss<d> projection_quadrature(n_qpoints);

  // wrap the vector in AnyData as used by deal's operators
  dealii::AnyData solution_data;
  solution_data.add(&solution, "solution");
  dealii::AnyData input_dummy;
  for(unsigned int l = 0; l < n_loops; ++l)
  {
    app.setup_system();
    app.setup_vector(solution);
    // would be better to use project_to_boundary but slightly more
    // complicated
    dealii::VectorTools::project(app.dofs(), app.hanging_nodes(),
                                 projection_quadrature, bdry_function,
                                 solution);
    newton.notify(dealii::Algorithms::Events::remesh);
    newton(solution_data, input_dummy);

    app.output_results(l, &solution_data);

    tr.refine_global(1);
  }
}

int main(int argc, const char** argv)
{
  std::ofstream logfile("deallog");
  dealii::deallog.attach(logfile);

  AmandusParameters param;

  param.enter_subsection("Application");
  param.declare_entry("Number of global refinement loops", "3");
  param.declare_entry("Number of quadrature points for boundary projection", "8");
  param.declare_entry("UMFPack", "true");
  param.leave_subsection();

  param.enter_subsection("Model");
  param.declare_entry("Eigenvalue", "2.0");
  param.declare_entry("Dimensionality", "2");
  param.leave_subsection();

  param.read(argc, argv);
  param.log_parameters(dealii::deallog);

  param.enter_subsection("Model");
  const unsigned int dim = param.get_integer("Dimensionality");
  param.leave_subsection();

  switch(dim)
  {
    case 2:
      run<2>(param);
      break;
    case 3:
      run<3>(param);
      break;
  }
}
