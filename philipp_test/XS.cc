#include <philipp_test/XS.h>

template<int dim>
XS<dim>::XS() {
}

template<int dim>
double XS<dim>::Source(const dealii::Point<dim>& r) const {
	return 1.0;
}

template class XS<2> ;
template class XS<3> ;
