#ifndef __LAPLACE2_XS_H
#define __LAPLACE2_XS_H

#include <deal.II/base/logstream.h>
#include <deal.II/base/point.h>
#include <math.h>
#include <iostream>

template <int dim>
class XS
{
  template <int> friend class MatrixIntegrator ;
  template <int> friend class RHSIntegrator ;
public:
  XS() ;
protected:
  double Source(const dealii::Point<dim>& r) const ;
};

#endif /* __LAPLACE_XS_H */
