#include <philipp_test/Laplace.h>

int main(int argc, const char** argv) {

	std::ofstream logfile("deallog");
	dealii::deallog.attach(logfile);
	dealii::deallog.depth_console(2);

	const int d = 2;

	XS<d> XSData;

	const std::string FiniteElementName = "FE_Q(1)";
	SpaceData<d> SpaceProblem;
	dealii::GridGenerator::hyper_cube(SpaceProblem.Triangulation, 0, 1);
	SpaceProblem.Triangulation.refine_global(4);
	SpaceProblem.FiniteElementName = FiniteElementName;

	Space<d> SpaceSolver(SpaceProblem, XSData);
	SpaceSolver.run();

}

