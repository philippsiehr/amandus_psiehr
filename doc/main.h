/**
 * @mainpage
 *
 * Amandus is a platform for solving mixed finite element
 * problems. The solvers are based on multilevel overlapping Schwarz
 * methods. Ideally, a program written with the help of Amandus
 * consists only of a short driver file and functions which integrate
 * a residual and a matrix on cells and faces of a mesh.
 *
 * The module @ref Examples contains such applications. It is suggested to
 * look there first. These examples use the integrator classes found
 * in the module @ref integrators.
 *
 * Please refer to the "Modules" tab above for a structured overview
 * of this package.
 */
