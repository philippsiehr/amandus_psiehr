/*
 * evproblem.h
 *
 *  Created on: 02.04.2015
 *      Author: psiehr
 */

#ifndef EVPROBLEM_H_
#define EVPROBLEM_H_

//
//#include <deal.II/base/logstream.h>
//#include <deal.II/integrators/laplace.h>
//#include <deal.II/meshworker/integration_info.h>
//#include <deal.II/meshworker/dof_info.h>
//#include <deal.II/fe/fe_tools.h>
//#include <deal.II/fe/fe_values.h>
//#include <deal.II/numerics/fe_field_function.h>
//#include <deal.II/algorithms/newton.h>
//
//#include <deal.II/integrators/l2.h>
//
//
//#include <amandus.h>
//#include <integrator.h>

template<int dim>
class tDatum
{
public:
   tDatum();
   ~tDatum();
 };


template<int dim>
tDatum<dim>::~tDatum()
{
}



//template<int dim>
//class EigenvalueProblem {
//public:
//	EigenvalueProblem();
//	void run();
//private:
	//SpaceData<dim> & SpaceProblem;
	//const XS<dim> & XSData;
//	boost::scoped_ptr<const dealii::FiniteElement<dim> > fe;
//	MatrixIntegrator<dim> Matrix;
//	RHSIntegrator<dim> RHS;
//	AmandusApplicationSparse<dim> app;
//	AmandusSolve<dim> solver;
//	AmandusResidual<dim> residual;
//	void VTKOutput();
//};


class MyClass
{
public:
  void foo();
  int bar;
};



#endif /* EVPROBLEM_H_ */
