#include <philipp_test/Space.h>

template<int dim>
Space<dim>::Space(SpaceData<dim> & SpaceProblem_, const XS<dim> & XSData_) :
		SpaceProblem(SpaceProblem_), XSData(XSData_), fe(
				dealii::FETools::get_fe_by_name<dim, dim>(
						SpaceProblem.FiniteElementName)), Matrix(XSData), RHS(
				XSData, SpaceProblem), app(SpaceProblem.Triangulation, *fe), solver(
				app, Matrix), residual(app, RHS) {
}

template<int dim>
void Space<dim>::run() {

    //boundarycondition
	app.set_boundary(0);

	app.control.set_reduction(1.e-10);
	app.control.log_history(true);
	app.control.log_result(false);
	//  app.refine_mesh(true);
	solver.notify(dealii::Algorithms::Events::remesh);
	app.setup_system();
	app.setup_vector(SpaceProblem.SpaceSolution);

	dealii::Vector<double> RHS_;
	app.setup_vector(RHS_);
	dealii::AnyData RHS;
	RHS.add(&RHS_, "Newton iterate");

	dealii::AnyData Residual;
	residual(RHS, Residual);

	dealii::AnyData SolverSolution;
	SolverSolution.add(&(SpaceProblem.SpaceSolution), "solution");

	//solver(SolverSolution, RHS);

	dealii::Algorithms::Newton<dealii::Vector<double> > newton(residual,
	                                                            solver);
	dealii::AnyData input_dummy;

	//RHS.input_vector_names.push_back("Newton iterate");
	newton.notify(dealii::Algorithms::Events::remesh);
	newton(SolverSolution, input_dummy);

	VTKOutput();

}

template<int dim>
void Space<dim>::VTKOutput() {
	dealii::DataOut<dim> data_out;

	dealii::DoFHandler<dim> dh(SpaceProblem.Triangulation);
	dh.distribute_dofs(*fe);

	data_out.attach_dof_handler(dh);
	data_out.add_data_vector(SpaceProblem.SpaceSolution, "solutionEIn");
	data_out.build_patches(fe->tensor_degree());

	std::ostringstream filename;
	filename << "solution.vtk";
	std::ofstream output(filename.str().c_str());
	data_out.write_vtk(output);

	data_out.clear();
	dh.clear();
}

// MatrixIntegrator-Klasse

template<int dim>
MatrixIntegrator<dim>::MatrixIntegrator(const XS<dim> & XSData_) :
		XSData(XSData_) {
}

template<int dim>
void MatrixIntegrator<dim>::cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
		dealii::MeshWorker::IntegrationInfo<dim>& info) const {
	//Eigenvalue
	const double lambda = 1.0;

	const unsigned int n_dofs = info.fe_values().dofs_per_cell;
	for (unsigned int k = 0; k < info.fe_values().n_quadrature_points; ++k) {
		const double dx = info.fe_values().JxW(k);
		for (unsigned int i = 0; i < n_dofs; ++i)
			for (unsigned int j = 0; j < n_dofs; ++j)
				dinfo.matrix(0, false).matrix(i, j) +=dx* ((info.fe_values().shape_grad_component(j, k,0)
						* info.fe_values().shape_grad_component(i, k, 0))
						+ lambda* (info.fe_values().shape_value_component(j, k, 0)
								* info.fe_values().shape_value_component(i, k, 0)));
	}
}

template<int dim>
void MatrixIntegrator<dim>::boundary(dealii::MeshWorker::DoFInfo<dim>& dinfo,
		typename dealii::MeshWorker::IntegrationInfo<dim>& info) const {
//
//// Anfang: Nitsch-Matrix-0-Rand
//	const unsigned int deg = info.fe_values(0).get_fe().tensor_degree();
//	dealii::LocalIntegrators::Laplace::nitsche_matrix(
//			dinfo.matrix(0, false).matrix, info.fe_values(0),
//			dealii::LocalIntegrators::Laplace::compute_penalty(dinfo, dinfo,
//					deg, deg));
//// ende





// const unsigned int deg = info.fe_values().get_fe().tensor_degree();
// const unsigned int n_dofs = info.fe_values().dofs_per_cell;
// for (unsigned int k=0; k<info.fe_values().n_quadrature_points; ++k)
//   {
//     const double dx = info.fe_values().JxW(k);
//     const dealii::Point<dim> &n = info.fe_values().normal_vector(k);
//     for (unsigned int i=0; i<n_dofs; ++i)
// 	for (unsigned int j=0; j<n_dofs; ++j)
// 	  dinfo.matrix(0,false).matrix(i,j) += dx *
// 	      ((2. * dealii::LocalIntegrators::Laplace::compute_penalty(dinfo, dinfo, deg, deg) *
// 	       info.fe_values().shape_value_component(i,k,0) *
// 	       info.fe_values().shape_value_component(j,k,0) -
// 	       ((n * info.fe_values().shape_grad_component(i,k,0)) *
// 	  	info.fe_values().shape_value_component(j,k,0) +
// 	  	(n * info.fe_values().shape_grad_component(j,k,0)) *
// 	  	info.fe_values().shape_value_component(i,k,0))));
//   }

//	const dealii::FEValuesBase<dim> &fe_v = info.fe_values();
//	dealii::FullMatrix<double> &local_matrix = dinfo.matrix(0).matrix;
//	dealii::Vector<double> &local_vector = dinfo.vector(0).block(0);

//   using namespace dealii;

//	const dealii::FEValuesBase<dim> &fe = info.fe_values();
//	Vector<double> &local_vector = dinfo.vector(0).block(0);


}

template<int dim>
void MatrixIntegrator<dim>::face(dealii::MeshWorker::DoFInfo<dim>& dinfo1,
		dealii::MeshWorker::DoFInfo<dim>& dinfo2,
		dealii::MeshWorker::IntegrationInfo<dim>& info1,
		dealii::MeshWorker::IntegrationInfo<dim>& info2) const {
}

// RHSIntegrator-Klasse

template<int dim>
RHSIntegrator<dim>::RHSIntegrator(const XS<dim> & XSData_,
		const SpaceData<dim> & SpaceProblem_) :
		XSData(XSData_), SpaceProblem(SpaceProblem_) {
}

template<int dim>
void RHSIntegrator<dim>::cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
		dealii::MeshWorker::IntegrationInfo<dim>& info) const {
//	const unsigned int n_dofs = info.fe_values().dofs_per_cell;
//	for (unsigned int k = 0; k < info.fe_values().n_quadrature_points; ++k)
//		for (unsigned int i = 0; i < n_dofs; ++i)
//			dinfo.vector(0).block(0)(i) += info.fe_values().JxW(k)
//					* (XSData.Source(info.fe_values().quadrature_point(k)))
//					* info.fe_values().shape_value_component(i, k, 0);

	std::cout<<info.values[0][0][0]<<std::endl;

//	dealii::LocalIntegrators::Laplace::cell_residual(dinfo.vector(0).block(0),
//	                                                         info.fe_values(0),
//	                                                         info.gradients[0][0]);
//	dealii::LocalIntegrators::L2::L2(dinfo.vector(0).block(0),
//			info.fe_values(0),
//			info.values[0][0],
//			-1.0 * 1.0); //1.0 is the eigenvalue

}

template<int dim>
void RHSIntegrator<dim>::boundary(dealii::MeshWorker::DoFInfo<dim>& dinfo,
		dealii::MeshWorker::IntegrationInfo<dim>& info) const {
	// If we use Nitsche's method of weak boundary conditio,n
	// we have to change the rhs with some additional information.
	// If we use pure dirichlet bc that will be:
	// (f,v)+Σ_(faces){γ(u_d,v)-(u_d,n*∇v)}

//	//These are the arguments of the nitsche-method
//	const unsigned int deg = info.fe_values(0).get_fe().tensor_degree();
//	dealii::Vector<double> &localvector = dinfo.vector(0).block(0);
//	const dealii::FEValuesBase<dim> &fe = info.fe_values(0);
//    double penalty=dealii::LocalIntegrators::Laplace::compute_penalty(dinfo, dinfo,	deg, deg);
//	double factor = 1;
//
//	const BoundaryValues<dim> boundaryobject;
//
//	std::vector<double> boundary_values(fe.n_quadrature_points);

//	//loop over all cells, faces etc
//	const unsigned int n_dofs=fe.dofs_per_cell;
//	for (unsigned int k=0; k<fe.n_quadrature_points; ++k)
//		for(unsigned int i; i<n_dofs; ++i ){
//			localvector(i)+=0.;
//		}
}

template<int dim>
void RHSIntegrator<dim>::face(dealii::MeshWorker::DoFInfo<dim>&,
		dealii::MeshWorker::DoFInfo<dim>&,
		dealii::MeshWorker::IntegrationInfo<dim>&,
		dealii::MeshWorker::IntegrationInfo<dim>&) const {
}

//Konstruktor
template<int dim>
BoundaryValues<dim>::BoundaryValues() : dealii::Function<dim>(1) {
}

//
template<int dim>
double BoundaryValues<dim>::value(const dealii::Point<dim> &p) const {
	return 0.;
}


// Sollte eigentlich vererbt werden

//template<int dim>
//void BoundaryValues<dim>::value_list(
//		const std::vector<dealii::Point<dim> > &points,
//		std::vector<double> &values, const unsigned int) const {
//	Assert(values.size()==points.size(),
//			ExcDimensionMismatch(values.size(),points.size()));
//	for (unsigned int i = 0; i < values.size(); ++i) {
//		values[i] = 0.; //ZeroBoundary
//	}
//}

template class Space<2> ;
template class Space<3> ;

