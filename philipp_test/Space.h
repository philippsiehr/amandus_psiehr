#ifndef __LAPLACE_SPACE_H
#define __LAPLACE_SPACE_H

#include <deal.II/base/logstream.h>
#include <deal.II/integrators/laplace.h>
#include <deal.II/meshworker/integration_info.h>
#include <deal.II/meshworker/dof_info.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/numerics/fe_field_function.h>
#include <deal.II/algorithms/newton.h>

#include <deal.II/integrators/l2.h>


#include <amandus.h>
#include <integrator.h>

#include <philipp_test/XS.h>

template<int dim>
struct SpaceData {
	dealii::Triangulation<dim> Triangulation;
	std::string FiniteElementName;
	dealii::Vector<double> SpaceSolution;
};


template<int dim>
class BoundaryValues: public dealii::Function<dim> {
public:
	BoundaryValues();
	virtual double value(const dealii::Point<dim> &p) const;

//	virtual void value_list(const std::vector<dealii::Point<dim> > &points,
//			std::vector<double> &values,
//			const unsigned int component = 0) const;
	//virtual ~BoundaryValues();
};


template<int dim>
class MatrixIntegrator: public AmandusIntegrator<dim> {
public:
	MatrixIntegrator(const XS<dim> & XSData_);
	virtual void cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
			dealii::MeshWorker::IntegrationInfo<dim>& info) const;
	virtual void boundary(dealii::MeshWorker::DoFInfo<dim>& dinfo,
			dealii::MeshWorker::IntegrationInfo<dim>& info) const;
	virtual void face(dealii::MeshWorker::DoFInfo<dim>& dinfo1,
			dealii::MeshWorker::DoFInfo<dim>& dinfo2,
			dealii::MeshWorker::IntegrationInfo<dim>& info1,
			dealii::MeshWorker::IntegrationInfo<dim>& info2) const;
	virtual ~MatrixIntegrator() {
	}
	;
private:
	const XS<dim> & XSData;
};

template<int dim>
class RHSIntegrator: public AmandusIntegrator<dim> {
public:
	RHSIntegrator(const XS<dim> & XSData_,
			const SpaceData<dim> & SpaceProblem_);
	virtual void cell(dealii::MeshWorker::DoFInfo<dim>& dinfo,
			dealii::MeshWorker::IntegrationInfo<dim>& info) const;
	virtual void boundary(dealii::MeshWorker::DoFInfo<dim>& dinfo,
			dealii::MeshWorker::IntegrationInfo<dim>& info) const;
	virtual void face(dealii::MeshWorker::DoFInfo<dim>& dinfo1,
			dealii::MeshWorker::DoFInfo<dim>& dinfo2,
			dealii::MeshWorker::IntegrationInfo<dim>& info1,
			dealii::MeshWorker::IntegrationInfo<dim>& info2) const;
	//virtual ~RHSIntegrator();  Dann Fehler in der Space Klasse (?)
private:
	const XS<dim> & XSData;
	const SpaceData<dim> & SpaceProblem;
};



template<int dim>
class Space {
public:
	Space(SpaceData<dim> & SpaceProblem_, const XS<dim> & XSData_);
	void run();
private:
	SpaceData<dim> & SpaceProblem;
	const XS<dim> & XSData;
	boost::scoped_ptr<const dealii::FiniteElement<dim> > fe;
	MatrixIntegrator<dim> Matrix;
	RHSIntegrator<dim> RHS;
	AmandusApplicationSparse<dim> app;
	AmandusSolve<dim> solver;
	AmandusResidual<dim> residual;
	void VTKOutput();
};

#endif /* __LAPLACE2_SPACE1_H */
