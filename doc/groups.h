/**********************************************************************
 *  Copyright (C) 2011 - 2014 by the authors
 *  Distributed under the MIT License
 *
 * See the files AUTHORS and LICENSE in the project root directory
 **********************************************************************/

/**
 * @defgroup apps Application classes and functions
 *
 * @brief The objects storing the stuff and doing the work
 */

/**
 * @defgroup integrators Local integrators
 *
 * @brief Local integrators for different models
 */

/**
 * @defgroup Examples Examples
 *
 * @brief Test programs that also serve as basis for implementing a model.
 */

/**
 * @defgroup Verification Verification
 *
 * @brief Small programs verifying the consistency of different parts of the library.
 */

