### Run
Assuming that amandus was built in `<path to amandus source>/build` run

  export DEAL_II_DIR=<path to deal.II build or install>
  export AMANDUS_DIR=<path to amandus source>
  mkdir build
  cd build
  cmake ..
  make && make test
